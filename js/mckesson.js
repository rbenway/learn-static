$(document).ready(function() {
	
    $('.launch.flash').click(function() {
        $('.modal').fadeIn(100);
        jwplayer().play();
    });

    $(".close.flash").click(function() {
        $('.modal').fadeOut(100);
        ga('send', 'event', 'Button', 'Click', 'Purchase Details');    	
        jwplayer().stop();
    });	
    
   $(".close.flash").click(function() {
        $('.modal').fadeOut(100);
        ga('send', 'event', 'Button', 'Click', 'Purchase Details');    	
        jwplayer().stop();
    });	
    
    $('.launch.html').click(function() {
        $('.modal').fadeIn(100);
        $('.video1')[0].play();
    });
    $(".close.html").click(function() {
    	$('.modal').fadeOut(100);
        $(".video1")[0].pause();
    });    
    
    /*$('.close').click(function() {
        $('.modal').fadeOut(100);
        ga('send', 'event', 'Button', 'Click', 'Purchase Details');
    });*/

    /*************************************************************
     *
     * Benefits Selector
     *`
     ************************************************************/

    $(function() {
        var c = 0;
        $("body").on('click', '.developers', function(e) {
            e.preventDefault();
            $(".arrow-up").animate({
                left : '-249px'
            }, 'slow');
            $(".developers_copy").css('display', 'table');
            $(".managers_copy").css('display', 'none');
            $(".operations_copy").css('display', 'none');
        });
    });

    $(function() {
        var c = 0;
        $("body").on('click', '.managers', function(e) {
            e.preventDefault();
            $(".arrow-up").animate({
                left : '0px'
            }, 'slow');
            $(".developers_copy").css('display', 'none');
            $(".managers_copy").css('display', 'table');
            $(".operations_copy").css('display', 'none');
        });
    });

    $(function() {
        var c = 0;
        $("body").on('click', '.operations', function(e) {
            e.preventDefault();
            $(".arrow-up").animate({
                left : '252px'
            }, 'slow');
            $(".developers_copy").css('display', 'none');
            $(".managers_copy").css('display', 'none');
            $(".operations_copy").css('display', 'table');
        });
    });

    var hash = window.location.hash;
    tmp = hash.split('?');
    hash = tmp[0];
    // if (window.location.pathname === '/') {
    // if (hash === '' & hash != '#developers') {
    // 	$(".arrow-up").animate({
    // 		left : '-=248px'
    // 	}, 'slow');
    // 	$(".developers_copy").css('display', 'table');
    // 	$(".managers_copy").css('display', 'none');
    // 	$(".operations_copy").css('display', 'none');
    //  }
    if (hash === '#developers' || hash === '' || hash === "#benefits" || hash === "#features" || hash === "#roadmap") {
        $(".arrow-up").animate({
            left : '-=248px'
        }, 'slow');
        $(".developers_copy").css('display', 'table');
        $(".managers_copy").css('display', 'none');
        $(".operations_copy").css('display', 'none');
    }
    if (hash === '#managers') {
        $(".arrow-up").animate({
            left : '0px'
        }, 'slow');
        $(".developers_copy").css('display', 'none');
        $(".managers_copy").css('display', 'table');
        $(".operations_copy").css('display', 'none');
    }

    if (hash === '#operations') {
        $(".arrow-up").animate({
            left : '252px'
        }, 'slow');
        $(".developers_copy").css('display', 'none');
        $(".managers_copy").css('display', 'none');
        $(".operations_copy").css('display', 'table');
    }


    var ben_class = '';
    var sp_class = '';

    $('ul#benefits_selectors li span').click(function() {
        ben_class = $(this).parent().attr('class');

        switch(ben_class) {

            case 'developers':
                window.location.hash = $(this).attr('data-href');
                break;

            case 'managers':
                window.location.hash = $(this).attr('data-href');
                break;

            case 'operations':
                window.location.hash = $(this).attr('data-href');
                break;

        }
    });

    /*************************************************************
     *
     * Features Selector
     *
     ************************************************************/

    $(function() {
        var c = 0;
        $(".experience_section").click(function() {
            $(".arrow-up").animate({
                left : '-278'
            }, 'slow');
            $(".experience_copy").css('display', 'table');
            $(".platform_copy").css('display', 'none');
            $(".core_copy").css('display', 'none');
        });
    });

    $(function() {
        var c = 0;
        $(".platform_section").click(function() {
            $(".arrow-up").animate({
                left : '-2px'
            }, 'slow');
            $(".experience_copy").css('display', 'none');
            $(".platform_copy").css('display', 'table');
            $(".core_copy").css('display', 'none');
        });
    });

    $(function() {
        var c = 0;
        $(".core_section").click(function() {
            $(".arrow-up").animate({
                left : '275px'
            }, 'slow');
            $(".experience_copy").css('display', 'none');
            $(".platform_copy").css('display', 'none');
            $(".core_copy").css('display', 'table');
        });
    });


    $('.pop').fadeOut(100);
    $('.core_list a').click(function() {
        $('.pop').fadeOut(100);
    });

    /*************************************************************
     *
     * Features Sub Selector
     *
     ************************************************************/

    $(".features_sub_nav li").click(function() {
        $(".features_sub_nav li").css("background-color", "#4890dc");
        $(this).css("background-color", "#4890dc");
    });

    $(".features_sub_nav li").click(function() {
        $(".features_sub_nav li").css("color", "#606060");
        $(this).css("color", "#fff");
    });

    /*************************************************************
     *
     * Features De Selector
     *
     **************************************************************/
    var getDiv = "";
    var pos = "";
    $(".exp_list ul li a").click(function() {
        pos = $(this).offset();
        $(".plat_list ul li .messagepop").parent().fadeOut(500);
        $(".layers_list ul li .messagepop").parent().fadeOut(500);
        getDiv = $(this).parent().find("div.messagepop");
        var adjustleft = pos.left - 130;
        switch(pos.top) {

            case 864:
                var adjusttop = pos.top - 794;
                break;

            case 896:
                var adjusttop = pos.top - 826;
                break;

            case 928:
                var adjusttop = pos.top - 868;
                break;

            case 960:
                var adjusttop = pos.top - 910;
                break;

            case 992:
                var adjusttop = pos.top - 932;
                break;

        }

        $(getDiv).parent().attr("style", "display: block; position: relative;  top: " + adjusttop + "px;");
        $(getDiv).fadeIn(500);
    });

    $(".plat_list ul li a").click(function() {

        pos = $(this).offset();
        $(".exp_list ul li .messagepop").parent().fadeOut(500);
        $(".layers_list ul li .messagepop").parent().fadeOut(500);
        getDiv = $(this).parent().find("div.messagepop");
        var adjustleft = pos.left - 414;
        switch(pos.top) {

            case 864:
                var adjusttop = pos.top - 804;
                break;

            case 896:
                var adjusttop = pos.top - 836;
                break;

            case 928:
                var adjusttop = pos.top - 868;
                break;

            case 960:
                var adjusttop = pos.top - 910;
                break;

            case 992:
                var adjusttop = pos.top - 932;
                break;

        }
        $(getDiv).parent().attr("style", "display: block; position: relative; top: " + adjusttop + "px;");
        $(getDiv).fadeIn(500);
    });

    $(".layers_list ul li a").click(function() {
        pos = $(this).offset();
        $(".plat_list ul li .messagepop").parent().fadeOut(500);
        $(".exp_list ul li .messagepop").parent().fadeOut(500);

        getDiv = $(this).parent().find("div.messagepop");
        var adjustleft = pos.left - 642;
        switch(pos.top) {

            case 864:
                var adjusttop = pos.top - 804;
                break;

            case 896:
                var adjusttop = pos.top - 836;
                break;

            case 928:
                var adjusttop = pos.top - 868;
                break;

            case 960:
                var adjusttop = pos.top - 910;
                break;

            case 992:
                var adjusttop = pos.top - 932;
                break;

        }
        $(getDiv).parent().attr("style", "display: block; position: relative; top: " + adjusttop + "px;");
        $(getDiv).fadeIn(500);
    });

    $('.close_btn').click(function() {
        $('#features ul li div.core_list ul li a').removeClass('selected');
    });

    //Deselect on page click
    $(document).mouseup(function(e) {
        var container = $(".messagepop");

        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.fadeOut(500);
        }
    });

    $(document).mouseup(function(e) {
        var container = $("#features ul li div.core_list ul li a");

        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.removeClass('selected');
        }
    });

    /*************************************************************
     *
     * Add Hash
     *
     ************************************************************/

    var hash = window.location.hash;
    if (hash != '') {
        hash = hash.replace('#', '');
        //$('.' + hash).children("span").end().children(".hover").fadeIn(0);
    }

    /*************************************************************
     *
     * Features Pop Message
     *
     ************************************************************/

    $(function() {
        $(".core_list ul li a").on('click', function() {
            var current = $(this);
            var name = current.attr('href');

            //Remove all
            $('.selected').removeClass('selected');

            //Add to current
            current.addClass("selected")

            return false;
        });

    });

    $('body').on('click', '.close_btn', function() {
        var id = '#' + $(this).parent().parent().parent().attr('id');
        $(id).fadeOut('slow');
    });

});

/*************************************************************
 *
 * Roadmap
 *
 ************************************************************/

var slideshows = $('.cycle-slideshow').on('cycle-prev cycle-next', function(e, opts) {
    // advance the other slideshow
    slideshows.not(this).cycle('goto', opts.currSlide);
    if(opts.currSlide==0){
        $('.cycle-prev').hide();
        setTimeout(function(){$('.cycle-carousel-wrap').css('left',0);},500);
    } else {
        $('.cycle-prev').show();
    }
});

var slideshows2 = $('.cycle-slideshow').on('cycle-pager-activated', function(e, opts) {
    // advance the other slideshow
    slideshows2.not(this).cycle('goto', opts.currSlide);
    if(opts.currSlide==0){
        $('.cycle-prev').hide();
        setTimeout(function(){$('.cycle-carousel-wrap').css('left',0);},500);
    } else {
        $('.cycle-prev').show();
    }
});

$('#cycle-2 .cycle-slide').click(function() {
    var index = $('#cycle-2').data('cycle.API').getSlideIndex(this);
    slideshows.cycle('goto', index);
    if(opts.currSlide==0){
        $('.cycle-prev').hide();
        setTimeout(function(){$('.cycle-carousel-wrap').css('left',0);},500);
    } else {
        $('.cycle-prev').show();
    }
});

window.onerror = function(message, url, lineNumber) {
    return true;
};

$(window).load(function() {
    $('#pager a').each(function(i) {
        $(this).css('background-image', 'url(gfx/timeline_gfx/link_' + i + '.png)');
    });

    // scroll to selected page
    $('header a').click(function() {
        var target = $(this).attr('href');
        var targetTop = $(target).offset().top - 100;
        setTimeout(function() {
            highlightCurrent($(this).index());
        }, 500);
        $('html:not(:animated),body:not(:animated)').animate({
            scrollTop : targetTop
        }, 500);
        if (target != 'contact') {
            return false;
        }
    });

});

$(window).load(function(){
    var target = window.location.href;
    tmp = target.split('?');
    target = tmp[0];
//alert(target);

    if(target.localeCompare('#features')){
        if($(target).length){
            var targetTop = $(target).offset();
            targetTop = targetTop.top - 100
        }
    }
    else if(target.localeCompare('#roadmap')){
        if($(target).length){
            var targetTop = $(target).offset();
            targetTop = targetTop.top - 100
        }
    }
    else if(target.localeCompare("#benefits")){
        if($(target).length){
            var targetTop = $(target).offset();
            targetTop = targetTop.top - 100
        }
    }
    else if(target.localeCompare('#developers')){
        if($(target).length){
            var targetTop = $('#benefits').offset().top - 100;
//alert(true);
        }
    }
    else if(target.localeCompare('#managers')){
        if($(target).length){
            var targetTop = $('#benefits').offset().top - 100;
        }
    }
    else if(target.localeCompare('#operations')){
        if($(target).length){
            var targetTop = $('#benefits').offset().top - 100;
        }
    } else{
        var targetTop = $(target).offset().top - 100;
    }
    setTimeout(function() {
        highlightCurrent($(this).index());
    }, 500);
    $('html:not(:animated),body:not(:animated)').animate({
        scrollTop : targetTop
    }, 500);
    if (target != 'contact') {
        return false;
    }


// if($(this).attr('href').localeCompare(window.location.hash)) {
// 				$('li.scroll > a').parent().attr('class', 'scroll');
// 				$(this).parent().attr('class', 'scroll active');
// 			}

});

$('li.scroll > a').click(function(){
    //alert($(this).attr('href'));
    if($(this).attr('href').localeCompare(window.location.hash)) {
        $('li.scroll > a').attr('class', '');
        $(this).attr('class', 'current');
    }
})


var hash = window.location.hash;
tmp = hash.split('?');
hash = tmp[0];
if(hash === '#developers'){
    $('html, body').animate({scrollTop: $('#benefits').offset().top-99}, 100);
//alert('true');
}
else if(hash === '#managers'){
    $('html, body').animate({scrollTop: $('#benefits').offset().top-99}, 100);
}
else if(hash === '#operations'){
    $('html, body').animate({scrollTop: $('#benefits').offset().top-99}, 100);
} else {
    if (hash === ''){
        //do nothing
    }else{
        $('html, body').animate({scrollTop: $(window.location.hash).offset().top - 100 }, 100);
    }
}

// set current link
var screenHeight = $(window).height();
$(window).scroll(function() {
    setTimeout(function() {
        var cutoff = $(window).scrollTop();
        //alert(cutoff);
        if (cutoff >= 497 && cutoff <= 1059) {
            highlightCurrent(0);
        } else if (cutoff >= 1060 && cutoff <= 1800) {
            highlightCurrent(1);
        } else if (cutoff >= screenHeight) {
            highlightCurrent(2);
        } else {
            $('nav li a').removeClass('current');
        }
    }, 50);
});

function highlightCurrent(n) {
    $('nav li a').removeClass('current');
    $('nav li:eq(' + n + ') a').addClass('current');
}

$('.cycle-next,.cycle-prev').click(function() {
    $('.cycle-next,.cycle-prev').css('z-index', '-1');
    setTimeout(function() {
        $('.cycle-next,.cycle-prev').css('z-index', '1');
    }, 100);
});
$('#pager').on('click','a',function() {
    $('.cycle-next,.cycle-prev').css('z-index', '-1');
    setTimeout(function() {
        $('.cycle-next,.cycle-prev').css('z-index', '1');
    }, 100);
});

$(".launch a").removeAttr("href");

/*if ($("html").hasClass("ie8")) {

    $('.launch').click(function() {
        $('.modal').fadeIn(100);
        jwplayer().play();
    });

    $(".close").click(function() {
        jwplayer().stop();
    });
} else {
    $('.launch').click(function() {
        $('.modal').fadeIn(100);
        $('#video1')[0].play();
    });
    $(".close").click(function() {
        $("#video1")[0].pause();
    });
}*/