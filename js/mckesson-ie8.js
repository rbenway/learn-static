$(document).ready(function(){
	var slideshows = $('.cycle-slideshow').on('cycle-prev cycle-next', function(e, opts) {
	    // advance the other slideshow
	    slideshows.not(this).cycle('goto', opts.currSlide);
	    if(opts.currSlide==0){
	        $('.cycle-prev').hide();
	        setTimeout(function(){$('.cycle-carousel-wrap').css('left',0);},500);
	    } else if(opts.currSlide==1){
	        setTimeout(function(){$('.cycle-carousel-wrap').css('left',-603.8);},500);
	    } else if(opts.currSlide==2){
	    	setTimeout(function(){$('.cycle-carousel-wrap').css('left',-1207.6);},500);
	    } else if(opts.currSlide==3){
	    	setTimeout(function(){$('.cycle-carousel-wrap').css('left',-1811.4);},500);
	    } else if(opts.currSlide==4){
	    	setTimeout(function(){$('.cycle-carousel-wrap').css('left',-2415.2);},500);
	    } else if(opts.currSlide==5){
	    	setTimeout(function(){$('.cycle-carousel-wrap').css('left',-3019);},500);
	    }
	    if(opts.currSlide!=0){
	        $('.cycle-prev').show();
	    }
	});
	var slideshows2 = $('.cycle-slideshow').on('cycle-pager-activated', function(e, opts) {
	    // advance the other slideshow
	    slideshows.not(this).cycle('goto', opts.currSlide);
	    if(opts.currSlide==0){
	        $('.cycle-prev').hide();
	        setTimeout(function(){$('.cycle-carousel-wrap').css('left',0);},500);
	    } else if(opts.currSlide==1){
	        setTimeout(function(){$('.cycle-carousel-wrap').css('left',-603.8);},500);
	    } else if(opts.currSlide==2){
	    	setTimeout(function(){$('.cycle-carousel-wrap').css('left',-1207.6);},500);
	    } else if(opts.currSlide==3){
	    	setTimeout(function(){$('.cycle-carousel-wrap').css('left',-1811.4);},500);
	    } else if(opts.currSlide==4){
	    	setTimeout(function(){$('.cycle-carousel-wrap').css('left',-2415.2);},500);
	    } else if(opts.currSlide==5){
	    	setTimeout(function(){$('.cycle-carousel-wrap').css('left',-3019);},500);
	    }
	    if(opts.currSlide!=0){
	        $('.cycle-prev').show();
	    }
	});
});