var meetTheTeamApp = angular.module('meetTeamApp',[
    'meetTeamApp.controllers'
]).
config(['$routeProvider',function($routeProvider) {
    $routeProvider
        .when('/', {templateUrl : 'members.html', controller  : 'MeetTheTeamCtrl' })
        .when('/member/:firstName/:lastName', { templateUrl : 'teamdetail.html',controller : 'TeamDetailCtrl'})
        .otherwise({redirectTo: '/'});
}]);

angular.module('meetTeamApp.controllers',[]).controller('MeetTheTeamCtrl',function($scope, $http) {
     $http.get('http://mhsihub-126.oc.mckesson.com/team/devs')
     .success(function (data) {
     $scope.membersList = data;
     })
     .error(function (data, status, headers, config) {
     $scope.errorMessage = "Couldn't load the list of development team developers, error # " + status;
     });
     $http.get('http://mhsihub-126.oc.mckesson.com/team/products')
     .success(function (data) {
     $scope.productList = data;
     })
     .error(function (data, status, headers, config) {
     $scope.errorMessage = "Couldn't load the list of product team members, error # " + status;
     });
}).controller('TeamDetailCtrl',function($scope, $http,$routeParams) {
    var firstName = $routeParams.firstName;
    var lastName = $routeParams.lastName;
    $http.get('http://mhsihub-126.oc.mckesson.com/team/member/'+firstName+'/'+lastName)
        .success(function (data) {
            $scope.member = data;
        })
});

function setBackgroundUrl(element, backgroundUrl, imgExtn){
        backgroundUrl = 'data/Images/'+ backgroundUrl + imgExtn;
        var urlString = 'url(' + backgroundUrl + ')';
        element.style.backgroundImage = urlString;
}
