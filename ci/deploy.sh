SSH_HOST=$1
BUILD_PATH=$WORKSPACE
TARGET_PATH=./target
ARCHIVE_NAME=$JOB_NAME-$BUILD_ID.tar.gz
ARCHIVE_PATH=${TARGET_PATH}/${ARCHIVE_NAME}
DEPLOY_PATH=/opt/magnolia/data/html

rm -rf ${TARGET_PATH} && mkdir ${TARGET_PATH}

echo tar -zcvf ${ARCHIVE_PATH} --exclude 'target/*' --exclude 'target' --exclude '*.tar.gz' --exclude '.git/*' --exclude '.git' --exclude 'ci/*' --exclude 'ci' -C ${BUILD_PATH} .
tar -zcvf ${ARCHIVE_PATH} --exclude 'target/*' --exclude 'target' --exclude '*.tar.gz' --exclude '.git/*' --exclude '.git' --exclude 'ci/*' --exclude 'ci' -C ${BUILD_PATH} .

echo scp ${ARCHIVE_PATH} ${SSH_USER}@${SSH_HOST}:
scp ${ARCHIVE_PATH} ${SSH_USER}@${SSH_HOST}:

echo ssh -t -t ${SSH_USER}@${SSH_HOST}
ssh -t -t ${SSH_USER}@${SSH_HOST} <<ENDSSH

rm -rf ${DEPLOY_PATH} && mkdir ${DEPLOY_PATH}

tar -zxvf ${ARCHIVE_NAME} -C ${DEPLOY_PATH}

rm -f ${ARCHIVE_NAME}

exit 0

ENDSSH